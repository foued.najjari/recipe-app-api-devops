
Paas Project
# Recipe App API 
 Build a [Backend REST API with Python & Django ]

                  using:

                    - Python
                    - Django / Django-REST-Framework
                    - Docker / Docker-Compose
                    - Test Driven Development



To start project, run:

```
docker-compose up
```

The API will then be available at http://127.0.0.1:8000
