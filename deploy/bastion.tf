#1..... select the instance informations 
data "aws_ami" "amazon_linux" {
  most_recent = true
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0.*-x86_64-gp2"]
  }
  owners = ["amazon"]
}
#3..CREATE ASSUME ROLE TO THE BASTION 
resource "aws_iam_role" "bastion" {
  name               = "${local.prefix}-bastion"
  assume_role_policy = file("./templates/bastion/instance-profile-policy.json")

  tags = local.common_tags
}

resource "aws_iam_role_policy_attachment" "bastion_attach_policy" {
  role       = aws_iam_role.bastion.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
}

resource "aws_iam_instance_profile" "bastion" {
  name = "${local.prefix}-bastion-instance-profile"
  role = aws_iam_role.bastion.name
}


resource "aws_instance" "bastion" {
  ami           = data.aws_ami.amazon_linux.id
  instance_type = "t2.micro"

  #2...create user data script to which install sdocker in each ec2instances created 
  user_data = file("./templates/bastion/user-data.sh")

  #3.. instance profile
  iam_instance_profile = aws_iam_instance_profile.bastion.name

  #4.bastion key name
  key_name = var.bastion_key_name

  #5.subnt we want ur aws instance to lauch into 
  subnet_id = aws_subnet.public_a.id # Cause want bastion instance accessible publicly
  #     so we can connect thing s we have from our local machine, then we can adminiter our network
  # bastion is not cfritical service , not respnsble to run appl, its just for admin perposes,
  #we can launch in one subnet at time 
  #so what could we do if we need to be able to access both AZs is create multi bastoins servers
  # and put one in a subnet in other in b subnet
  # and if the netwrook the AZ is down  , we need access bastion in other AZ, we can just change it 
  # to public_b and redeployed and recreate our basion instance 

  #6. security grooup adding port 22 443 80 / private subnet a-b to bastion server 
  vpc_security_group_ids = [
    aws_security_group.bastion.id
  ]


  #3.tags
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-bastion")
  )

}
##  6. add security group to bastion instance to 

resource "aws_security_group" "bastion" {
  description = "Control bastion inbound and outbound access"
  name        = "${local.prefix}-bastion"
  vpc_id      = aws_vpc.main.id

  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }
  ########  443 & 80 required for pulling images from ECR
  ## entire outbound access from server is not recommnded , cause if an attacker goes into the box 
  # they can connect to any server , so we want to limit them as much as possible ,so they can only 
  # connect to the minimal things 
  # here if attack so they onlu=y be able to connect  out using those two ports which would limit  
  # what their capabilities was 
  # the bastion will can access only the database in 
  egress { ## 
    from_port = 5432
    to_port   = 5432
    protocol  = "tcp"
    cidr_blocks = [                    # our internal network can access only this port 5432 
      aws_subnet.private_a.cidr_block, #if ther is any app run inside netwk , bastion will connect to it if we add the port egrerss
      aws_subnet.private_b.cidr_block,
    ]
  }
  # tags
  tags = local.common_tags
}
