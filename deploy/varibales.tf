variable "prefix" {
  default = "raad"
}

#adding tags:
variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "foued.najjari@gmail.com"
}

###### vars for RDS database instance 
# we can enter them manually by terminal, useful many devs shatring thesame dev env
#necause dont want to change titbased on what all the dirfferent user per in 
variable "db_username" {
  description = "Username for the RDS Postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}
#4 BASTION key name
variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}
#5. create task definition, 
variable "ecr_image_api" {
  description = "ECR Image for API"
  default     = "359380704752.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR Image for API"
  default     = "359380704752.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}
