resource "aws_db_subnet_group" "main" {
  name = "${local.prefix}-main"
  subnet_ids = [
    aws_subnet.private_a.id,
    aws_subnet.private_b.id
  ]

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-main")
  )
}
######################
#6.create and deploy a service 
# wwe need to add a new securtiy group for our ecs cluster 
resource "aws_security_group" "rds" {
  description = "Allow access to the RDS database instance."
  name        = "${local.prefix}-rds-inbound-access"
  vpc_id      = aws_vpc.main.id

  ingress {
    protocol  = "tcp"
    from_port = 5432 # default 
    to_port   = 5432

    # 6. we can limited down further to restricted to just the access from our bastion server
    security_groups = [
      aws_security_group.bastion.id, #6.create and deploy a service 
      # wwe need to add a new securtiy group for our ecs cluster 
      aws_security_group.ecs_service.id, # SO WE can give access to our ecs service from our databse 
      # cause our service need to be able to access our databse when it's running our applications

    ]

  }

  tags = local.common_tags
}
#####################
resource "aws_db_instance" "main" { # we can have multiple db , so we define the main one 
  identifier              = "${local.prefix}-db"
  name                    = "recipe" # => dataabase name within our instance 
  allocated_storage       = 20       # disk space , GB 
  storage_type            = "gp2"    # cheapest one in aws 
  engine                  = "postgres"
  engine_version          = "11.4"
  instance_class          = "db.t2.micro"
  db_subnet_group_name    = aws_db_subnet_group.main.name
  password                = var.db_password
  username                = var.db_username
  backup_retention_period = 0 # => number of days to setup backup for,
  # 0 because its just a demo , and to reduce costs 
  multi_az = false # recoimmended  the db run in multi AZ, 
  #false to reduce cost
  skip_final_snapshot = true # if we destroy the db , aws create a "final 
  #snapshot+dbID "" ( the 1st time is ok but then if recreate it said "snapshot id in use !" 
  #so false to not create any snapshot  to easily destroy and create db , ) of the data in 
  # the db to protect it, the issur in terraform , there needs to be a unique name available 
  # the db snapshot a, terraform easily cereate destroy modify infra 
  vpc_security_group_ids = [aws_security_group.rds.id] # set permission on accessing   database 
  #throught the netowr

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-main")
  )
}
