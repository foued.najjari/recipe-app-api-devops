resource "aws_ecs_cluster" "main" {
  name = "${local.prefix}-cluster"

  tags = local.common_tags
}
#2.for running the service itself  
#   
#  • need STS assume role to the task used to assume the role which defined in the last task role policy file
# this we're gonna use in future , to assign the task execution role , which gives ECS the permissions
#that it needs in order to start a new task  ( taskrole.json to retrive images from ecr and logs perm)
#  
resource "aws_iam_policy" "task_execution_role_policy" {
  name        = "${local.prefix}-task-exec-role-policy"
  path        = "/"
  description = "Allow retrieving images and adding to logs"
  policy      = file("./templates/ecs/task-exec-role.json")
}
# define IAM ROle
# when we create our task definition ,in future , we  can then reference this to set task execution role
resource "aws_iam_role" "task_execution_role" {
  name               = "${local.prefix}-task-exec-role"
  assume_role_policy = file("./templates/ecs/assume-role-policy.json")
}
# attach the policy'(previous one that was created ) to the role 
resource "aws_iam_role_policy_attachment" "task_execution_role" {
  role       = aws_iam_role.task_execution_role.name
  policy_arn = aws_iam_policy.task_execution_role_policy.arn
}
# until here, we assigning the permissions in order to start tasks, / start the containers 


#this iam role is for giving PERmissions to our task that it needs at run-time 
# permission that docker container needs after it started / for running the containers 
resource "aws_iam_role" "app_iam_role" {
  name               = "${local.prefix}-api-task"
  assume_role_policy = file("./templates/ecs/assume-role-policy.json")

  tags = local.common_tags
}
# 3. add log group
resource "aws_cloudwatch_log_group" "ecs_task_logs" {
  name = "${local.prefix}-api"

  tags = local.common_tags
}
#5. create a task defintion 
#    • now we will define our task definition within our ECS terraform ,
# so weill wreate a template file ressource for our containers definition 

data "template_file" "api_container_definitions" {
  template = file("templates/ecs/container-definitions.json.tpl")

  vars = {
    app_image         = var.ecr_image_api
    proxy_image       = var.ecr_image_proxy
    django_secret_key = var.django_secret_key
    db_host           = aws_db_instance.main.address
    db_name           = aws_db_instance.main.name
    db_user           = aws_db_instance.main.username
    db_pass           = aws_db_instance.main.password
    log_group_name    = aws_cloudwatch_log_group.ecs_task_logs.name
    log_group_region  = data.aws_region.current.name
    allowed_hosts     = "*"

  }
}

resource "aws_ecs_task_definition" "api" {
  family                   = "${local.prefix}-api"
  container_definitions    = data.template_file.api_container_definitions.rendered
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = 256
  memory                   = 512
  execution_role_arn       = aws_iam_role.task_execution_role.arn
  task_role_arn            = aws_iam_role.app_iam_role.arn
  volume {
    name = "static"
  }
  tags = local.common_tags
}

#6.CREATE and deploy service :
# add security group give the appropriate access to our service that it needs to network 
#this allow   our service access to network to pull our container down and run it 
resource "aws_security_group" "ecs_service" {
  description = "Access for the ECS service"
  name        = "${local.prefix}-ecs-service"
  vpc_id      = aws_vpc.main.id
  #allow outbound access from our container on port 443
  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  #allow outbound access from our container on port 5432 of databse 
  egress {
    from_port = 5432
    to_port   = 5432
    protocol  = "tcp"
    cidr_blocks = [
      aws_subnet.private_a.cidr_block,
      aws_subnet.private_b.cidr_block,
    ]
  }
  #accept inbound connection from public internet 
  ingress {
    from_port   = 8000
    to_port     = 8000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = local.common_tags
}
#6. create our service 
resource "aws_ecs_service" "api" {
  name            = "${local.prefix}-api"              # on the cluster ecs
  cluster         = aws_ecs_cluster.main.name          # cluster name 
  task_definition = aws_ecs_task_definition.api.family # pass task definition by family name created above
  desired_count   = 1                                  # THE number of task we desire to run inside the service , ay increased to H.Avail, HISGHT THE COST ALSO 
  launch_type     = "FARGATE"                          # a servless docker deployment process #             this service type allow to run tasks without managing the servers that tunning them  # net conf , setting it in public now and later when we set LoadBalnc we will move it into private subnet   #cause we want to test it before we deploy it 

  network_configuration {
    subnets = [
      aws_subnet.public_a.id,
      aws_subnet.public_b.id,
    ]
    security_groups  = [aws_security_group.ecs_service.id] # assign a puyblic ip address to the service # when we lanch it 
    assign_public_ip = true
  }
}
