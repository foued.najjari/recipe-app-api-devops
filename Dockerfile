FROM python:3.7-alpine
# 
LABEL maintainer="fouad.najjari.IaasPaasProject"
# did not touched
ENV PYTHONUNBUFFERED 1
# added
ENV PATH="/scripts:${PATH}"

RUN pip install --upgrade pip

# not added

COPY ./requirements.txt /requirements.txt
RUN apk add --update --no-cache postgresql-client jpeg-dev
RUN apk add --update --no-cache --virtual .tmp-build-deps \
      gcc libc-dev linux-headers postgresql-dev musl-dev zlib zlib-dev
RUN pip install -r /requirements.txt
RUN apk del .tmp-build-deps

RUN mkdir /app
WORKDIR /app
COPY ./app /app
# added
COPY ./scripts/ /scripts/
RUN chmod +x /scripts/*
# not added

RUN mkdir -p /vol/web/media
RUN mkdir -p /vol/web/static
RUN adduser -D user
RUN chown -R user:user /vol/
RUN chmod -R 755 /vol/web
USER user
# added
CMD ["entrypoint.sh"]


